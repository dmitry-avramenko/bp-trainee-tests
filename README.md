# List of tasks

Look at one of the following tasks:

* [Is My Friend Cheating?](is_my_friend_cheating.md)
* [Matrix Determinant](matrix_determinant.md)
* [Twice Linear](twice_linear.md)
* [Valid Braces](valid_braces.md)
